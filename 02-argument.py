#! /usr/bin/pỳhton3

import argparse
parser = argparse.ArgumentParser(description="Programa exemple \
de processar args.", prog="02-arguments.py", epilog="hasta luego lucas.")

parser.add_argument("-e", "--edat", type=int, dest="useredat", help="edat a processar")
parser.add_argument("-f", "--fit", type=str, dest="fitxer", help="fitxer a processar", metavar="fileIn")

args = parser.parse_args()
print(parser)
print(args)

print(args.fitxer, args.useredat)
exit(0)
