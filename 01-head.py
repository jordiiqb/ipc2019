#! /bin/python3

import sys

f = sys.stdin
is_file = False

if len(sys.argv) == 2:
    file = sys.argv[1]
    is_file = True

f= file
if is_file:
    f = open(file, mode='r')
i = 0
while i < 10:
    print(f.readline())
    i += 1
if is_file:
    f.close()

#-------------------------------------------------------------------------------

# CORRECCIÓ EDUARD

# /usr/bin/python
#-*- coding: utf-8-*-
#
# head [file]
#  10 lines, file o stdin
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

#import sys
#MAXLIN=10
#fileIn=sys.stdin
#if len(sys.argv)==2:
#  fileIn=open(sys.argv[1],"r")
#counter=0
#for line in fileIn:
#  counter+=1
#  print (line, end=" ")
#  if counter==MAXLIN: break
#fileIn.close()
#exit(0)
#-------------------------------------------------------------------------------
