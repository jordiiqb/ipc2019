# /usr/bin/python
#-*- coding: utf-8-*-
# ps-server-one2one.py
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE
from datetime import datetime
parser = argparse.ArgumentParser(description="""PS server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()

HOST = ''
PORT = args.port


def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  sys.exit(0)

pid=os.fork()
if pid !=0:
  print("Engegat el server PS:", pid)
  sys.exit(0)

signal.signal(signal.SIGUSR1,mysigusr1)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)


while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  dateTimeObj = datetime.now()
  logFileName = f"{dateTimeObj.strftime('%y%d%m')}-{dateTimeObj.strftime('%H%M%S')}.log"
  file = open(f"./{logFileName}", "w")
  while True:
      data = conn.recv(1024)
      if not data: break
      file.write(str(data))
  file.close()
  conn.close()
