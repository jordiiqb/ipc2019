# /usr/bin/pyhton3
#-*-coding: utf-8-*-
#
# 12-popen-sql.py
#
# su -l postgres
# /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
#
# # psql -qTA -F';' -h 172.17.0.2 -U edtasixm06
#
#
# Es pot fer una primera versió tot hardcoded sense diàleg amb:
# psql -qtA -F';' lab_clinic -c “select * from pacients;”.
#
# Executa la consulta “select * from clients;” usant psq.
# El popem executa “psql -qtA -F';' lab_clinic” que deixa una connexió oberta
# i cal escriure la consulta hardcoded a fer i llegir la resposta del subprocés.
#
#     Fer els grant all on database clinic i gran select on table taula
#
#     Atenció als enter i al \q per finalitzar.
#
#   Atenció: posar al popen shell=True
# ------------------------------------------------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# ------------------------------------------------------------------------------

import sys,argparse
from subprocess import Popen, PIPE

# ------------------------------------------------------------------------------

command = ["psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"select * from oficinas;\"", args.ruta]
pipeData = Popen(command, stdout=PIPE, shell=True)

for line in pipeData.stdout:
    print(line.decode("utf-8")[:-1])

exit(0)
