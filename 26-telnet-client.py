# /usr/bin/python
#-*- coding: utf-8-*-
# 26-telnet-client.py -p port -s server
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

import sys,socket,os,signal,argparse

parser = argparse.ArgumentParser(description="""Telnet client""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-s","--server",type=str, default='')
args=parser.parse_args()

HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:

    command = input("INTRODUZCA COMANDO: ")
    if not command : break

    s.sendall(command.encode())

    while True:
        data = s.recv(1024)
        if data[-1:] == chr(4).encode():
            print(str(data[:-1]))
            break
        print(str(data))
s.close()
sys.exit(0)
