#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] -f file
#  5,10 o 15 lines i obligatori file per paràmetre
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# $ head.py -n 5 file.txt
# $ head.py -n 10 file.txt
# $ head.py -n 15 file.txt
# tots els altres casos d'error
# -------------------------------------

import argparse
parser = argparse.ArgumentParser(prog="04-head-choices.py")

parser.add_argument("-n", "--nlin", type=int, dest="nlin",\
                    default=10, choices=[5,10,15])
parser.add_argument("-f", "--fit", type=str, dest="fitxer", required=True)

args = parser.parse_args()

MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line, end=' ')
  if counter==MAXLIN: break
fileIn.close()
exit(0)
