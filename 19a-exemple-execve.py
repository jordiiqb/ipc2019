# /usr/bin/pyhton3
#-*- coding: utf-8 -*-
#
# 19a-exemple-execve.py
# Ídem anterior però ara el programa fill execula un “ls -la /”. Executa un nou
# procés carregat amb execv. Aprofitar per veure les fiferents variants de exec.
#
# ------------------------------------------------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# ------------------------------------------------------------------------------

import sys, os, signal

print("Hola, començament del programa principal.")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    sys.exit(0)

print("Programa Fill", os.getpid(), pid)
os.execve("/bin/bash",["/bin/bash","/home/users/inf/hisx2/isx48172244/Documents/show.sh"], {"nom":"joan","edat":"25"})

print("Hasta luego Lucas!")
sys.exit(0)
