# /usr/bin/pyhton3
#-*- coding: utf-8 -*-
#
# 18-fork-signal.py
# Usant el programa d'exemple fork fer que el procés fill (un while infinit) es
# governi amb senyals. Amb siguser1 mostra "hola radiola" i amb sigusr2 mostra
# "adeu andreu" i finalitza.
#
# ------------------------------------------------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# ------------------------------------------------------------------------------

import sys, os, signal

def printloop(signum,frame):
    print(f"HOLA RADIOLA.")

def endloop(signum,frame):
    print(f"Adéu Andreu.")
    sys.exit(1)

signal.signal(signal.SIGUSR2,endloop) #14
signal.signal(signal.SIGUSR1,printloop)

print("Hola, començament del programa principal.")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
    print("Programa Pare IS DEAD.", os.getpid(), pid)
else:
    print("Programa Fill IS ALIVE", os.getpid(), pid)
    while True:
        pass

print("Hasta luego Lucas!")
sys.exit(0)
