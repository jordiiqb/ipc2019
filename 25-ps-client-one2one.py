# /usr/bin/python
#-*- coding: utf-8-*-
# ps-client-one2one-pissara.py
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""PS server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("server",type=str, default='')
args=parser.parse_args()

HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

command = "ps"
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
    s.send(line)
s.close()
sys.exit(0)
