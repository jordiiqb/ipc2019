#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] [-f file ...] 
#  5,10 o 15 lines, n file sent n (0, inf)
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# $ head.py -n 5 -f file.txt
# $ head.py -n 10 -f file.txt
# $ head.py -n 15 -f file.txt
# $ head.py -n 5 -f file1.txt file2.txt
# $ head.py -v -n 5 -f file.txt
# tots els altres casos d'error
# -------------------------------------

import argparse

# Funció head

def head(fitxer):

    if args.verbose:

        print(f"\n{fitxer} --------------------------")

    fileIn=open(fitxer,"r")
    counter=0
    for line in fileIn:
      counter+=1
      print(line, end='')
      if counter==MAXLIN: break
    fileIn.close()

    return

parser = argparse.ArgumentParser(prog="04-head-choices.py")

parser.add_argument("-n", "--nlin", type=int, dest="nlin", \
                    default=10, choices=[5,10,15])
parser.add_argument("-f", "--fit", type=str, dest="fitList", nargs="*")

parser.add_argument("-v", dest="verbose", action='store_true')

args = parser.parse_args()

MAXLIN=args.nlin

for fit in args.fitList:

    head(fit)

exit(0)
