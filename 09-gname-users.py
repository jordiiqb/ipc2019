#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# 09-gname-users.py   [-s login|gid|gname]  -u fileusers -g fileGroup
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

# -------------------------------------

import sys, argparse
groupDict={}

parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
parser.add_argument("-s","--sort",type=str,\
        help="sort criteria: login | gid", metavar="criteria",\
        choices=["login","gid","gname"],dest="criteria")
parser.add_argument("-u", "--userFile",type=str,\
        help="user file (/etc/passwd style)", metavar="file", required=True)
parser.add_argument("-g", "--groupFile",type=str,\
        help="user file (/etc/group style)", metavar="file", required=True)
args=parser.parse_args()

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    self.gname=groupDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, uid:%d, gid=%d" % \
           (self.login, self.passwd, self.uid, self.gid, self.ganame,\
           self.gecos, self.home, self.shell))
  def sumaun(self):
    "funcio tonta que suma un al uid"
    self.uid+=1
  def __str__(self):
    "functió to_string"
    return("%s %d %d" % (self.login, self.uid, self.gid))

class UnixGroup():

    """
    Classe grup de unix: prototipus /etc/group
    gname:passwd:gid:listusers
    """

    def __init__(self, groupLine):
        "construcotr de un UnixGroup donada una línia del /etc/group"
        groupField = groupLine[:-1].split(":")
        self.gname = groupField[0]
        self.passwd = groupField[1]
        self.gid = int(groupField[2])
        self.userListStr = groupField[3]
        self.userList=[]
        if self.userListStr[:-1]:
            self.userList = self.userListStr[:-1].split(",")


    def __str__(self):
        "funció to string"
        return("%s %s %d %s" %(self.ganem, self.passwd, self.gid, self.userList))

# Comparadors


groupFile=open(args.groupFile, "r")
for line in groupFile:
    group=UnixGroup(line)
    groupDict[group.gid]=group
groupFile.close()


userFile=open(args.userFile,"r")
userList = []
for line in userFile:
    user=UnixUser(line)
    userList.append((user.login, user))

userFile.close()

userList.sort()

print(userList)
