# /usr/bin/python
#-*- coding: utf-8-*-
#
# 22-daytime-client.py
# -------------------------------------
# @edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

import sys,socket

HOST = '3.9.180.57'
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
    data = s.recv(1024)
    print('Received', repr(data))
    if not data: break
s.close()
sys.exit(0)
