# /usr/bin/python
#-*- coding: utf-8-*-
#
# 21-exemple-echo-server.py
# -------------------------------------
# @edt ASIX M06 Curs 2019-2020
# Gener 2018
# -------------------------------------

import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)   # activamos que empiece a escuchar at infinitum
conn, addr = s.accept()
print("Connected by", addr)
while True:
  data = conn.recv(1024)
  if not data: break
  conn.send(data)
conn.close()
sys.exit(0)
