# /usr/bin/pyhton3
#-*- coding: utf-8 -*-
#
# 16-signal.py segons
#
# Deifineix alarma(n segons). siguser1 incrementa 1 minut,
# siguser2 decrementa1 minut, sighub reinicia el compte,
# sigterm mostra quants segons falten de alarma,
# no és interrumpible amb control c.
# Sigalarm  mostra el valor de upper (quants hem incrementat)
# i down (quants decrementat) i acaba.
# ------------------------------------------------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# ------------------------------------------------------------------------------

import sys, os, signal, argparse

parser = argparse.ArgumentParser(description="Script señal alarma.")
parser.add_argument('segundos', type=int, help="Segundos restantes hasta que \
                                                suene la alarma.")
args = parser.parse_args()

nup = 0
ndown = 0

def endalarm(signum,frame):
    print(f"Signal handler called with signal: {signum}")
    print(f"  ¡FIN DE LA ALARMA!\n\tNúmero de uppers: {nup}.\n\tNúmero de downs:  {ndown}.")
    sys.exit(1)

def uptime(signum,frame):
    current_time = signal.alarm(0)
    signal.alarm(current_time+60)
    global nup
    nup += 1
    print(f"Tiempo incrementado 1min.")

def downtime(signum,frame):
    current_time = signal.alarm(0)
    if (current_time-60) < 0:
        signal.signal(signal.SIGALRM,endalarm)
    else:
        signal.alarm(current_time-60)
        global ndown
        ndown += 1
        print(f"Tiempo decrementado 1min.")

def resettime(signum,frame):
    signal.alarm(args.segundos)
    print(f"Alarma restablecida a {args.segundos} segundos.")

def showtime(signum,frame):
    current_time = signal.alarm(0)
    signal.alarm(current_time)
    print(f"Quedan {current_time} segundos para que suene la alarma.")


signal.signal(signal.SIGALRM,endalarm) #14
signal.signal(signal.SIGUSR1,uptime) #10
signal.signal(signal.SIGUSR2,downtime) #12
signal.signal(signal.SIGHUP,resettime) #1
signal.signal(signal.SIGTERM,showtime) #15
signal.signal(signal.SIGINT,signal.SIG_IGN)

signal.alarm(args.segundos)
print(os.getpid())

while True:
    pass

sys.exit(0)
