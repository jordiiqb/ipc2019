# /usr/bin/python
#-*- coding: utf-8-*-
# 26-telnetServer.py [-p port] [-d debug]
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Telnet server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-d","--debug",type=str, default="")
args=parser.parse_args()

HOST = ''
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:

    conn, addr = s.accept()
    print("Se ha establecido la conexión.")
    while True:
        command = conn.recv(1024)
        command = command.decode()
        print(command)
        if not command: break
        pipeData = Popen(command,shell=True,stdout=PIPE,stderr=True)
        for line in pipeData.stdout:
            conn.sendall(line)
        conn.sendall(chr(4).encode())

    conn.close()
